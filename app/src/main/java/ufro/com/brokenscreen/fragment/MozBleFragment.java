package ufro.com.brokenscreen.fragment;

import android.util.Log;

import ufro.com.brokenscreen.interfaces.IMozBleWrapper;
import ufro.com.mozbiisdkandroid2.OnMozbiiListener;

/**
 * Created by Usagi on 2017/10/26.
 */

public abstract class MozBleFragment extends BaseFragment implements OnMozbiiListener {
    final private String TAG = MozBleFragment.class.getSimpleName();
    protected IMozBleWrapper wrapper;

    public void setMozBleWrapper(IMozBleWrapper wrapper){
        this.wrapper = wrapper;
        this.wrapper.setOnMozbiiListener(this);
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        if(wrapper != null) {
            if(wrapper.isConnected()){
                wrapper.disConnect();
            }

            wrapper.setOnMozbiiListener(null);
        }
        super.onDestroy();
    }

}
