package ufro.com.brokenscreen.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.concurrent.ThreadLocalRandom;

import ufro.com.brokenscreen.manager.Constant;

/**
 * Created by Usagi on 2018/1/27.
 */

public class BrokenView extends View{
    private final String TAG = BrokenView.class.getSimpleName();
    private final float MAX_SCALE = 4f;
    private final float MIN_SCALE = 0.3f;

    private Canvas buffCanvas;
    private Bitmap buffBitmap;
    private Matrix matrix;

    public BrokenView(Context context){
        super(context);
    }

    public BrokenView(Context context, AttributeSet attr){
        super(context, attr);
        if(matrix == null){
            matrix = new Matrix();
        }
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(buffBitmap != null) {
            Log.v(TAG, "onDraw");
            canvas.drawBitmap(buffBitmap, 0, 0, null);
        }
    }

    public void addBroken(Bitmap bitmap){
        if(buffCanvas == null || buffBitmap == null){
            buffBitmap = Bitmap.createBitmap(Constant.SCREEN_WIDTH, Constant.SCREEN_HEIGH, Bitmap.Config.ARGB_8888);
            buffCanvas = new Canvas(buffBitmap);
        }
        randomMatrix(bitmap.getWidth(), bitmap.getHeight());
        buffCanvas.drawBitmap(bitmap, matrix, null);
        invalidate();
    }

    private void randomMatrix(int width, int height){
        matrix.reset();
        float cw = width / 2;
        float ch = height / 2;
        float scale = randomScale();
        float rotate = randomRotate();
        float dx = randomNumber(Constant.SCREEN_WIDTH);
        float dy = randomNumber(Constant.SCREEN_HEIGH);
        matrix.postScale(scale, scale, cw, ch);
        matrix.postRotate(rotate, cw, ch);
        matrix.postTranslate(dx - width /  2, dy - height / 2);
//        matrix.postTranslate(Constant.SCREEN_WIDTH/2 - width/2, Constant.SCREEN_HEIGH/2 - height/2);
    }

    private float randomScale(){
        float randomNumber = (float)ThreadLocalRandom.current().nextDouble(MIN_SCALE, MAX_SCALE);
        Log.v(TAG, "Random scale: " + randomNumber);
        return randomNumber;
    }

    private float randomRotate(){
        float randomNumber = (float)ThreadLocalRandom.current().nextDouble(0, 360);
        Log.v(TAG, "Random rotate: " + randomNumber);
        return  randomNumber;
    }

    private float randomNumber(int range){
        float randomNumber = (float)ThreadLocalRandom.current().nextDouble(  range * 0, range);
        Log.v(TAG, "Random number: " + randomNumber);
        return randomNumber;
    }

    public void reset(){
        if(buffBitmap != null) {
            buffBitmap.eraseColor(Color.TRANSPARENT);
            invalidate();
        }
    }

}
