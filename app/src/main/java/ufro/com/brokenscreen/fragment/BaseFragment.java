package ufro.com.brokenscreen.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ufro.com.brokenscreen.interfaces.OnFragmentFuncListener;


/**
 * Created by Usagi on 2017/10/26.
 */

public abstract class BaseFragment extends Fragment {
    protected ViewGroup container;
    protected OnFragmentFuncListener onFragmentFuncListener;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        if(null == this.container){
            this.container = (ViewGroup) inflater.inflate(getResId(), null, false);

            findView(this.container);
            setupViewComponent();
        } else{
            ViewGroup viewGroup = (ViewGroup) this.container.getParent();
            if(null != viewGroup){
                viewGroup.removeView(this.container);
            }
        }

        return this.container;
    }


    protected <T extends View> T findViewTById(View parent, int id){
        return (T)parent.findViewById(id);
    }

    public void setOnFragmentFuncListener(OnFragmentFuncListener listener){
        onFragmentFuncListener = listener;

    }

    protected abstract int getResId();
    protected abstract void findView(ViewGroup viewGroup);
    protected abstract void setupViewComponent();


}
