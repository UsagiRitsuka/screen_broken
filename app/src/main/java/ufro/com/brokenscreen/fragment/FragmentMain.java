package ufro.com.brokenscreen.fragment;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ThreadLocalRandom;

import ufro.com.brokenscreen.R;
import ufro.com.brokenscreen.databinding.FragmentMainBinding;
import ufro.com.brokenscreen.manager.Constant;
import ufro.com.brokenscreen.manager.DialogManager;
import ufro.com.brokenscreen.manager.FileManager;
import ufro.com.brokenscreen.manager.MyAudioManager;
import ufro.com.brokenscreen.manager.MyLog;
import ufro.com.brokenscreen.manager.ThreadManager;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Usagi on 2017/10/26.
 */

public class FragmentMain extends MozBleFragment{
    final private String TAG = FragmentMain.class.getSimpleName();
    private FragmentMainBinding binding;
    private boolean isSkipFirstColorDetect = false;
    private ProgressDialog progressDialog;
    private boolean isScaning = false;
    private boolean isMozBleInit = false;
    private File imgFile;
    private AlertDialog phtoDialog;
    private Button btnGallery;
    private Button btnCamera;

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(imgFile != null && imgFile.exists()){
            imgFile.delete();
        }
    }

    @Override
    protected void findView(ViewGroup viewGroup) {
        binding = DataBindingUtil.bind(viewGroup);
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View dialogView = layoutInflater.inflate(R.layout.photo_type_choose, viewGroup, false);
        phtoDialog = DialogManager.getCustomDialog(getContext(), dialogView);

        btnGallery = dialogView.findViewById(R.id.from_gallery);
        btnCamera = dialogView.findViewById(R.id.from_camera);
    }

    @Override
    protected void setupViewComponent() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.loading_msg));

        checkAndStartScan();

        binding.ble.setOnClickListener(view -> checkAndStartScan());
        binding.recover.setOnClickListener(view -> reset());
        binding.camara.setOnClickListener(view -> {
            phtoDialog.show();
            phtoDialog.getWindow().setLayout((int)(Constant.SCREEN_WIDTH * 0.8), (int)(Constant.SCREEN_HEIGH * 0.4));
        });

        binding.touchFrame.setOnClickListener(view -> {
            trigger();
        });

        binding.share.setOnClickListener(view -> {
            File file = new File(Constant.imageFilePath + "/broken.png");
            progressDialog.setCancelable(false);
            progressDialog.show();

            ThreadManager.getInstance().postToBackgroungThread(() -> {

                Bitmap bitmap = convertViewToBitmap(binding.brokenView,
                        Constant.SCREEN_WIDTH,
                        (int)(Constant.SCREEN_HEIGH * 0.9));

                storeDrawing(bitmap, file);
                ThreadManager.getInstance().postToUIThread(() -> {
                    shareWithAttachment(file);
                    progressDialog.dismiss();
                });
            });
        });

        btnGallery.setOnClickListener(view -> {
            openGallery();
            phtoDialog.dismiss();
        });
        btnCamera.setOnClickListener(view -> {
            pickFromCamera();
            phtoDialog.dismiss();
        });

    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, Constant.SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if(resultCode == RESULT_OK) {
            if (requestCode == Constant.SELECT_PHOTO) {
                try {
                    Uri selectedImage = intent.getData();
                    File file = FileManager.getFileFromUri(selectedImage, getContext());
                    Log.v(TAG, "PATH: " + file == null ? "null" : file.getAbsolutePath());
                    Glide.with(this).load(file).into(binding.bg);
                    reset();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == Constant.PICK_IMG_CAMERA) {
                if (imgFile != null && imgFile.exists()) {
                    Glide.with(getContext()).load(imgFile).into(binding.bg);
                    reset();
                }
            }
        }
    }

    @Override
    protected int getResId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onMozbiiConnected() {
        ThreadManager.getInstance().postToUIThread(() -> {
            Glide.with(this).load(R.drawable.ble_connect).into(binding.ble);
        });

        isScaning = false;
        MyLog.v(TAG, "onMozbiiConnected");
    }

    @Override
    public void onMozbiiDisconnected() {
        ThreadManager.getInstance().postToUIThread(() -> {
            Glide.with(this).load(R.drawable.ble_unconnect).into(binding.ble);
        });
        isScaning = false;
        isSkipFirstColorDetect = false;
        MyLog.v(TAG, "onMozbiiDisconnected");

        checkAndStartScan();
    }

    @Override
    public void onMozbiiColorDetected(int i) {
        MyLog.v(TAG, "onMozbiiColorDetected");
        if(!isSkipFirstColorDetect){
            isSkipFirstColorDetect = true;
        } else {
            ThreadManager.getInstance().postToUIThread(() -> trigger());
        }
    }

    private void checkAndStartScan(){
        if(wrapper.isEnable()) {
            isScaning = true;
            if(!isMozBleInit){
                wrapper.initMozBle();
                isMozBleInit = true;
            }
            wrapper.startScan();
        } else {
            isScaning = false;
            isMozBleInit = false;
            Toast.makeText(getContext(), R.string.ble_not_enable_hint, Toast.LENGTH_SHORT).show();
        }
    }

    private void trigger(){
        int resId = randomBrokenImage();
        Glide.with(getContext()).asBitmap().load(resId).into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, Transition<? super Bitmap> transition) {
                binding.myBrokenView.addBroken(resource);
                playBroken();
            }
        });
    }

    private int randomBrokenImage(){
        int resId = R.drawable.broken_1;
        int randomNumber = ThreadLocalRandom.current().nextInt(3);
        Log.v(TAG, "randomNumber: " + randomNumber);
        switch (randomNumber){
            case 0:
                resId = R.drawable.broken_1;
                break;
            case 1:
                resId = R.drawable.broken_2;
                break;
            case 2:
                resId = R.drawable.broken_3;
                break;
        }

        return resId;
    }

    private void playBroken(){
        int randomNumber = ThreadLocalRandom.current().nextInt(3);
        switch (randomNumber){
            case 0:
                MyAudioManager.getInstance().playShortAudio(getContext(), R.raw.sound_1);
                break;
            case 1:
                MyAudioManager.getInstance().playShortAudio(getContext(), R.raw.sound_2);
                break;
            case 2:
                MyAudioManager.getInstance().playShortAudio(getContext(), R.raw.sound_3);
                break;
        }

    }

    private void reset(){
        binding.myBrokenView.reset();
    }

    private void shareWithAttachment(File image) {
        Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        emailIntent.setType("image/png");
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "My Workbook");
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Look! I colored this!");
        emailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(image));
        startActivity(Intent.createChooser(emailIntent, "Share..."));
    }

    private void storeDrawing(Bitmap bitmap, File file) {
        if (file.exists()) {
            if (file.delete()) {
                MyLog.v(TAG, "delete success");
            } else {
                MyLog.v(TAG, "delete fail");
            }
        }

        OutputStream output;
        try {
            output = new FileOutputStream(file);

            // Compress into png format image from 0% - 100%
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
            output.flush();
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Bitmap convertViewToBitmap(View view, int bitmapWidth, int bitmapHeight){
        Bitmap bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
        view.draw(new Canvas(bitmap));

        return bitmap;
    }

    public void pickFromCamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(
                        getContext(),
                        "com.ufro.broken_screen",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, Constant.PICK_IMG_CAMERA);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an imageFromCamera file name
        String imageFileName = "ufro_broken_img";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        imgFile = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        //currentPhotoPath = imageFromCamera.getAbsolutePath();
        return imgFile;
    }
}
