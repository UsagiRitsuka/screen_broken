package ufro.com.brokenscreen.interfaces;


import ufro.com.brokenscreen.fragment.BaseFragment;

/**
 * Created by Usagi on 2017/10/26.
 */

public interface OnFragmentFuncListener {
    void startFragment(BaseFragment fragment);
    void popBackStack();
}
