package ufro.com.brokenscreen.interfaces;

import ufro.com.mozbiisdkandroid2.OnMozbiiListener;

/**
 * Created by Usagi on 2017/10/26.
 */

public interface IMozBleWrapper {
    void initMozBle();
    boolean isConnected();
    void setOnMozbiiListener(OnMozbiiListener listener);
    void startScan();
    void stopScan();
    void disConnect();
    boolean autoConnect();
    boolean isEnable();
    boolean isBleStateOn();
}
