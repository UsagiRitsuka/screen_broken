package ufro.com.brokenscreen.manager;

import android.util.Log;

/**
 * Created by Usagi on 2017/2/2.
 */

public class MyLog {
    final static private String TAG = MyLog.class.getSimpleName();
    static public void v(String tag, String msg){
        Log.v(tag, msg);
    }

    static public void v(String msg){
        Log.v(TAG, msg);
    }

    static public void d(String tag, String msg){
        Log.d(tag, msg);
    }

    static public void d(String msg){
        Log.d(TAG, msg);
    }

    static public void e(String tag, String msg){
        Log.e(tag, msg);
    }

    static public void e(String msg){
        Log.e(TAG, msg);
    }
}
