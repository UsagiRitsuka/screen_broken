package ufro.com.brokenscreen.manager;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;

/**
 * Created by Usagi on 2018/1/27.
 */

public class DialogManager {
    static public AlertDialog getCustomDialog(Context context, View customView){
        AlertDialog alertDialog = new AlertDialog.Builder(context).setView(customView).create();
        return alertDialog;
    }
}
