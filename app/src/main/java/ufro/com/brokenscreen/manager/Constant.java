package ufro.com.brokenscreen.manager;

/**
 * Created by Usagi on 2017/4/11.
 */

public class Constant {
    static public boolean UI_DEBUG = true;
    static public final int SELECT_PHOTO = 1000;
    static final public int PICK_IMG_CAMERA = 2000;

    static public int SCREEN_WIDTH = -1;
    static public int SCREEN_HEIGH = -1;
    static public String imageFilePath = null;
}
