package ufro.com.brokenscreen.manager;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.SoundPool;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Usagi on 2017/3/30.
 */

public class MyAudioManager {
    final private String TAG = MyAudioManager.class.getSimpleName();
    private MediaPlayer mediaPlayer;
    private MediaRecorder mediaRecorder;
    private boolean isRecording = false;
    private SoundPool soundPool;
    private int numOfMaxStream = 1; // for soundPool同時播放數量

    private static MyAudioManager myAudioManager;
    private String audioPath = "";

    private MyAudioManager(){

    }

    static public MyAudioManager getInstance(){
        if(myAudioManager == null){
            myAudioManager = new MyAudioManager();
        }

        return myAudioManager;
    }

    private void initSoundPool(){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            soundPool = (new SoundPool.Builder()).setMaxStreams(numOfMaxStream).build();
        }else{
            soundPool = new SoundPool(numOfMaxStream, AudioManager.STREAM_MUSIC, 5);
        }
    }

    public void setSoundPoolMaxStreams(int maxStreams){
        numOfMaxStream = maxStreams;
        initSoundPool();
    }


    public void playShortAudio(String srcPath){
        if(soundPool == null){
            initSoundPool();
        }

        int srcId = soundPool.load(srcPath, 1);
        MyLog.v(TAG, "id: " + srcId);
        soundPool.setOnLoadCompleteListener((soundPool1, sampleId, status) -> {
            if(status == 0) {
                MyLog.v(TAG, "sampleId: " + sampleId);
                soundPool1.play(sampleId, 1, 1, 1, 0, 1);
            }
        });
    }

    public void playShortAudio(Context context, int resId){
        if(soundPool == null){
            initSoundPool();
        }

        int srcId = soundPool.load(context, resId, 1);
        MyLog.v(TAG, "id: " + srcId);
        soundPool.setOnLoadCompleteListener((soundPool1, sampleId, status) -> {
            if(status == 0) {
                MyLog.v(TAG, "sampleId: " + sampleId);
                soundPool1.play(sampleId, 1, 1, 1, 0, 1);
            }
        });
    }


    public void startRecording(){
        if(isRecording){
            stopRecording();
        }

        isRecording = true;
        audioPath = FileManager.AUDIO_PATH + "/" + new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".mp3";
        File file = new File(FileManager.AUDIO_PATH);
        if(!file.exists()){
            file.mkdir();
        }

        mediaRecorder = new MediaRecorder();
        mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        mediaRecorder.setOutputFile(audioPath);
        mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

        MyLog.v(TAG, audioPath);

        try {
            mediaRecorder.prepare();
        } catch (IOException e){
            e.printStackTrace();
        }

        mediaRecorder.start();
    }

    public void stopRecording(){
        if(isRecording){
            isRecording = false;
            mediaRecorder.stop();
            mediaRecorder.release();
        }
    }

    /**
     * 取得最近一次錄音的路徑
     * @return
     */
    public String getAudioPath(){
        return audioPath;
    }
}
