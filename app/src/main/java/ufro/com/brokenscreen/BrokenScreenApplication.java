package ufro.com.brokenscreen;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import java.io.File;

import ufro.com.brokenscreen.manager.Constant;

/**
 * Created by Usagi on 2018/1/21.
 */

public class BrokenScreenApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(this);
        if(Constant.imageFilePath == null) {
            File file = new File(getExternalFilesDir(null).getAbsolutePath() + "/images");
            if (!file.exists()) {
                file.mkdir();
            }
            Constant.imageFilePath = file.getPath();
        }
    }
}
