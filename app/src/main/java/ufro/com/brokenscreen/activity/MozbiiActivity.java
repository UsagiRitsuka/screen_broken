package ufro.com.brokenscreen.activity;

import ufro.com.brokenscreen.interfaces.IMozBleWrapper;
import ufro.com.mozbiisdkandroid2.MozbiiBleWrapper;
import ufro.com.mozbiisdkandroid2.OnMozbiiListener;

/**
 * Created by Usagi on 2017/9/15.
 */

public abstract class MozbiiActivity extends PermissionActivity implements IMozBleWrapper {
    private MozbiiBleWrapper wrapper;


    @Override
    protected void setupViewComponent() {
        super.setupViewComponent();

    }

    protected void init(){
        if(wrapper == null){
            wrapper = new MozbiiBleWrapper(this);
//            if(!wrapper.isBleEnable()){
//                wrapper.enableBle();
//            }
        }

    }

    @Override
    public boolean isConnected() {
        boolean isConnected = false;
        if(wrapper != null){
            isConnected = wrapper.isConnected();
        }

        return isConnected;
    }

    @Override
    public void startScan() {
        if(wrapper != null) {
            wrapper.startScan();
        }
    }

    @Override
    public void stopScan() {
        if(wrapper != null) {
            wrapper.stopScan();
        }
    }

    @Override
    public void setOnMozbiiListener(OnMozbiiListener onMozbiiListener) {
        if(wrapper != null){
            wrapper.setOnMozbiiListener(onMozbiiListener);
        }

    }



    @Override
    public boolean autoConnect() {
        boolean isAutoConnect = false;
        if(wrapper != null){
            isAutoConnect = wrapper.autoConnect();
        }

        return isAutoConnect;
    }

    @Override
    public void disConnect() {

    }

    @Override
    public boolean isEnable() {
        boolean isEnable = false;
        if(wrapper != null){
            isEnable = wrapper.isBleEnable();
        }

        return isEnable;
    }

    @Override
    public boolean isBleStateOn() {
        boolean isBleStateOn = false;
        if(wrapper != null){
            isBleStateOn = wrapper.isBleStateOn();
        }
        return isBleStateOn;
    }

    @Override
    public void initMozBle() {
        if(wrapper != null) {
            wrapper.init();
        }
    }
}
