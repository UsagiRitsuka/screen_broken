package ufro.com.brokenscreen.activity;

import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;

import ufro.com.brokenscreen.manager.Constant;
import ufro.com.brokenscreen.manager.MyLog;


/**
 * Created by Usagi on 2017/8/28.
 */

public abstract class BaseActivity extends AppCompatActivity {
    final private String TAG = BaseActivity.class.getSimpleName();


    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        MyLog.v(TAG, "setContentView");
        findView();
        setupViewComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        MyLog.v(TAG, "onResume");
        if(Constant.SCREEN_WIDTH == -1 || Constant.SCREEN_HEIGH == -1){
            DisplayMetrics dm = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(dm);
            Constant.SCREEN_HEIGH = dm.heightPixels;
            Constant.SCREEN_WIDTH = dm.widthPixels;
        }
    }

    @Override
    protected void onDestroy() {
        MyLog.v(TAG, "onDestroy");
        super.onDestroy();
    }

    protected abstract void startApp();
    protected abstract void findView();
    protected abstract void setupViewComponent();
}
