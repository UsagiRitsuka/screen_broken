package ufro.com.brokenscreen.activity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import ufro.com.brokenscreen.R;
import ufro.com.brokenscreen.fragment.BaseFragment;
import ufro.com.brokenscreen.fragment.FragmentMain;
import ufro.com.brokenscreen.interfaces.OnFragmentFuncListener;
import ufro.com.brokenscreen.manager.ThreadManager;

public class MainActivity extends MozbiiActivity implements OnFragmentFuncListener{
    private ConstraintLayout splashView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void startApp() {
        init();
        ThreadManager.getInstance().postToUIThread(() -> {
            Animation am = new AlphaAnimation(1.0f, 0.0f);
            am.setDuration(3000);
            am.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    splashView.setVisibility(View.GONE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            splashView.startAnimation(am);

            FragmentMain fragmentChecker = new FragmentMain();
            fragmentChecker.setOnFragmentFuncListener(this);
            fragmentChecker.setMozBleWrapper(this);

            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.frame, fragmentChecker, fragmentChecker.getClass().getSimpleName())
                    .commitAllowingStateLoss();
        }, 2000);
    }

    @Override
    protected void findView() {
        splashView = findViewById(R.id.splash);
    }

    @Override
    protected void setupViewComponent() {
        super.setupViewComponent();
    }

    @Override
    public void popBackStack() {

    }

    @Override
    public void startFragment(BaseFragment fragment) {

    }
}
